<?php

namespace App;

use App\Exercise\Address;
use App\Exercise\Person;

require_once 'vendor/autoload.php';

$address1 = new Address("26 rue de la Republique", "Lyon", "France");
$address2 = new Address("166 cours Tolstoi", "Villeurbanne", "France");
$person = new Person("Kali Tar", 21, 200);
$person->addAddress($address1);
$person->addAddress($address2);
echo $person->listAddress();