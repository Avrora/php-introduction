<?php

$tab = ['ga', 'bu', 'zo', 'meu'];
//On push dans le tableau soit avec la fonction array_push
array_push($tab, 'valeur');
//soit directement avec cette syntaxe
$tab[] = 'blup';
//pour accéder à une valeur du tableau, c'est comme en JS avec l'index
echo $tab[3];
//un peu comme console.log mais en php (détaille le contenu d'une variable)
var_dump($tab);

/**
 * On peut faire des tableaux dit associatifs. En gros à la place 
 * d'utiliser un index numérique comme dans un tableau normal, on 
 * utilisera un index en chaîne de caractère pour l'associer à 
 * une valeur. C'est très rapidement comme les objet {} en js, mais
 * pas trop quand même, pasque ça reste un tableau en php, pas un objet
 */
$assoc = ['prenom' => 'Jean', 'nom' => 'Demel'];
echo $assoc['prenom'];
$assoc['age'] = 30;
var_dump($assoc);

for ($x=0; $x < 10; $x++) { 
    echo $x;
}

$count = 0;
while($count < 10)  {
    echo 'bloup';
    $count++;

}

/**
 * Pour parcourir un tableau (associatif ou non), on utilise le mot clef
 * foreach qui attend en premier argument le tableau à parcourir, puis le
 * mot clef as puis une variable qui contiendra l'index de chaque élément
 * du tableau puis une variable qui contiendra la valeur de chaque élément
 * du tableau
 */
foreach ($assoc as $index => $item) {
    echo '<p>' . $index . ' : ' . $item . '</p>';
}

/* on utilise la fonction count pour avoir la taille d'un tableau 
    (c'est comme le array.length en JS en gros)
*/
echo count($tab);