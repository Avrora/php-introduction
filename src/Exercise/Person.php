<?php

namespace App\Exercise;

class Person {
    public $name;
    public $age;
    public $money;
    private $addresses = [];

    public function __construct(string $name, int $age, float $money) {
        $this->name = $name;
        $this->age = $age;
        $this->money = $money;
        //ili this->addresses = []; Togda ybiraem iz nachala " = [];"
    }

    public function addAddress(Address $address):void {
        array_push($this->addresses, $address);
        //$this->addresses[] = $address;
    }

    public function listAddress():string {
        //echo "<html></html>"; Sozdaem element html, no eto ne obyazatelno
        $list = "<ul>"; 
       
        foreach ($this->addresses as $key => $address) {
            $list .= "<li>$address->street $address->city $address->country</li>";
           // $list .= "<li>";
           // $list .= $address->street . " ";
           // $list .= $address->scity . " ";
           // $list .= $address->country . " ";
           // $list .= "</li>";
        }

        $list .= "</ul>";
        return $list;
    }
}

    /* SDELAT FUNCTZIU DLYA OTOBRAZENIYA PERSON
    public function addPerson(Person $address):void {
        array_push($this->addresses, $address);
        //$this->addresses[] = $address;
    }

    public function listAddress():string {
        //echo "<html></html>"; Sozdaem element html, no eto ne obyazatelno
        $list = "<ul>"; 
       
        foreach ($this->addresses as $key => $address) {
            $list .= "<li>$address->street $address->city $address->country</li>";
           // $list .= "<li>";
           // $list .= $address->street . " ";
           // $list .= $address->scity . " ";
           // $list .= $address->country . " ";
           // $list .= "</li>";
        }

        $list .= "</ul>";
        return $list;
    }

*/