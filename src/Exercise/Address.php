<?php

namespace App\Exercise;

class Address {
    public $street;
    public $city;
    public $country;

    public function __construct(string $street, string $city, string $country) {
        $this->street = $street;
        $this->city = $city;
        $this->country = $country;
    }
}