<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A basic form</title>
</head>
<body>
    <form action="form-submit-example.php" method="post">
        <input type="text" name="test">
        <button>Submit</button>
    </form>
</body>
</html>