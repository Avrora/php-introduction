<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Address</title>
</head>
<body>
    <form action="exercise-form-submit.php" method="post">
        <p><input type="text" name="person_name" placeholder="First name, last name"></p>
        <p><input type="number" name="person_age" placeholder="Age"></p>
        <p><input type="number" name="person_money" placeholder="Money"></p>
        <p><input type="text" name="address_street" placeholder="Address (№, street)"></p>
        <p><input type="text" name="address_city" placeholder="City"></p>
        <p><input type="text" name="address_country" placeholder="Country"></p>
        <p><button>Submit</button></p>
    </form>
</body>
</html>