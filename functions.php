<?php


echo maFonction('coucou');

/**
 * Les fonction marche de la même manière qu'en JS à la différence
 * qu'avec PHP on va pouvoir typer les argument et le retour de la fonction.
 * Ici, l'argument $arg doit être de type string, si on appelle la fonction
 * en lui donnant n'importe quoi d'autre qu'une string, la fonction
 * plantera sans même essayer d'aller plus loin.
 * Derrière les argument, on rajoute :string pour dire que la fonction
 * DEVRA avoir un return de type chaîne de caractère. Si la fonction
 * n'a pas de return ou bien un return d'un type différent, la fonction
 * plantera.
 * Ce typage va nous permettre d'être plus stricts et carrés sur les définitions
 * et l'utilisation de nos fonctions ainsi que de forcer certain comportement
 */
function maFonction(string $arg):string {
    return 'bloup ' . $arg;
}

/**
 * le :void indique que la fonction ne devra pas avoir de return sous
 * peine de planter.
 */
function fonctionQuiRenvoieRien():void {
    echo 'je ne renvoie rien';
}


/**
 * On peut ajouter le contenu d'un autre fichier dans le fichier actuel
 * en utilisant soit include, soit require, soit include_once, soit require_once
 * include : On ajoute le contenu du fichier, si celui ci n'existe pas, il
 * y aura un warning mais le code ne plantera pas et continuera son exécution
 * require : On ajoute le contenu du fichier, si celui ci n'existe pas, il
 * y aura une fatal error, le code plante et l'exécution s'arrête
 * include_once / require_once : même comportement respectif sauf que
 * si le fichier à déjà été include/require, il ne le refera pas une 
 * autre fois
 */
// include_once 'loops.php';
// include_once 'loops.php';