<?php

// echo 'coucou tout le monde';

echo "<p>bloup</p>";

$maVariable = 'Ma Valeur';

$maVariable = 'changer';

//On concatène soit avec le point 
echo 'du texte ' . $maVariable;
//soit directement dans la chaîne de caractère si elle utilise des guillemets
echo "autre $maVariable";

/**
 * Les if-else c'est pareil qu'en JS sauf que le elseif on peut ne pas
 * mettre d'espace...
 */
if($maVariable === 'bloup') {
    echo 'youpi';
}elseif($maVariable === 'changer') {
    echo 'piyou';
}else {
    echo 'autre';
}